﻿var apiCity = "http://apiadvisor.climatempo.com.br/api/v1/locale/city?name=";
var apiWeatherNow = "http://apiadvisor.climatempo.com.br/api/v1/weather/locale/";
var apiWeatherDays = "http://apiadvisor.climatempo.com.br/api/v1/forecast/locale/";
var tokenCity = "&token=30b9f7c286b7e698db99b91f4c6b487c";
var tokenWeatherNow = "/current?token=30b9f7c286b7e698db99b91f4c6b487c";
var tokenWeatherDays = "/days/15?token=30b9f7c286b7e698db99b91f4c6b487c";
var id = "";

function forecastWeek() {
    var searchWeatherDays = apiWeatherDays + id + tokenWeatherDays;
    $.ajax({
        type: 'GET',
        url: searchWeatherDays,
        data: { get_param: 'value' },
        dataType: 'json',
        success: function (API) {
            var minMax = new Array(6);
            var state = new Array(6);
            var today = new Date();
            var day = today.getDay();
            var week = new Array(6);
            week[0] = 'Dom';
            week[1] = 'Seg';
            week[2] = 'Ter';
            week[3] = 'Qua';
            week[4] = 'Qui';
            week[5] = 'Sex';
            week[6] = 'Sáb';

            function daysOfWeek() {
                if (day == 6) {
                    day = 0;
                }
                else {
                    day++;
                }
            }

            for (var i = 0; i < 7; i++) {
                minMax[i] = API.data[i].temperature.min + "°/" + API.data[i].temperature.max + "°";
                state[i] = "/Images/70/" + API.data[i].text_icon.icon.day + ".png";
            }

            $("#date0").text(week[day]);
            $("#temp0").text(minMax[0]);
            document.getElementById('img0').src = state[0];
            $("#img0").fadeIn();

            daysOfWeek();
            $("#date1").text(week[day]);
            $("#temp1").text(minMax[1]);
            document.getElementById('img1').src = state[1];
            $("#img1").fadeIn();

            daysOfWeek();
            $("#date2").text(week[day]);
            $("#temp2").text(minMax[2]);
            document.getElementById('img2').src = state[2];
            $("#img2").fadeIn();

            daysOfWeek();
            $("#date3").text(week[day]);
            $("#temp3").text(minMax[3]);
            document.getElementById('img3').src = state[3];
            $("#img3").fadeIn();

            daysOfWeek();
            $("#date4").text(week[day]);
            $("#temp4").text(minMax[4]);
            document.getElementById('img4').src = state[4];
            $("#img4").fadeIn();

            daysOfWeek();
            $("#date5").text(week[day]);
            $("#temp5").text(minMax[5]);
            document.getElementById('img5').src = state[5];
            $("#img5").fadeIn();

            daysOfWeek();
            $("#date6").text(week[day]);
            $("#temp6").text(minMax[6]);
            document.getElementById('img6').src = state[6];
            $("#img6").fadeIn();
        }
    });
}

function forecastNow() {
    var searchWeatherNow = apiWeatherNow + id + tokenWeatherNow;
    $.ajax({
        type: 'GET',
        url: searchWeatherNow,
        data: { get_param: 'value' },
        dataType: 'json',
        success: function (information) {
            var sensationNow = information.data.sensation;
            if (-10 < sensationNow && sensationNow <= 2) {
                document.body.className = "freeze";
            }
            if (2 < sensationNow && sensationNow <= 16) {
                document.body.className = "cold";
            }
            if (16 < sensationNow && sensationNow <= 26) {
                document.body.className = "regular";
            }
            if (26 < sensationNow) {
                document.body.className = "hot";
            }

            $("#condition").text(information.data.condition);
            var temperatureString = information.data.temperature + "°";
            $("#temperature").text(temperatureString);
            var humidityString = "Umidade: " + information.data.humidity + "%";
            $("#humidity").text(humidityString);
            var sensationString = "Sensação térmica: " + information.data.sensation + "°";
            $("#sensation").text(sensationString);
            var windString = "Velocidade do vento: " + information.data.wind_velocity + " km/h";
            $("#wind_velocity").text(windString);
            var pressureString = "Pressão atmosférica: " + information.data.pressure + " hPa";
            $("#pressure").text(pressureString);
        }
    });
}

function IDCidade() {
    var city = cityBox.value.toLowerCase();
    var searchCity = apiCity + city + tokenCity;
    $.ajax({
        type: 'GET',
        url: searchCity,
        data: { get_param: 'value' },
        dataType: 'json',
        success: function (response) {
            if (response.length == 0) {
                $("#buttonAlert").addClass('show');
                $('.show').show();
                $('.informations').css("display", "none");
            }
            else {
                $('.show').hide();
                $('.informations').css("display", "block");
                id = response[0].id;
                forecastNow();
                forecastWeek();
            }
        }
    });
}

function actions() {
    $("#button").click(function () {
        IDCidade();
    });
}

$(document).ready(function () {
    actions();
});